from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from django.utils.translation import gettext_lazy as _

from api.serializers import FinanceUserSerializer


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    default_error_messages = {
        'no_active_account': _(
            'No active account found with the given credentials / Please verify your account before proceeding'
        )
    }

    def validate(self, attrs):
        data = super().validate(attrs)

        serializer = FinanceUserSerializer(self.user).data
        for k, v in serializer.items():
            data[k] = v


        if self.user.username:
            data["username"] = self.user.username.split('@')[0]
        else:
            data["username"] = self.user.email.split('@')[0]
        return data


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer
