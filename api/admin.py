from django.contrib import admin
from api.models import FinanceUser


# Register your models here.
admin.site.register(FinanceUser)