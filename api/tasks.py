import datetime

from api.models import AmountDetails


def update_interest_task(amounts):
    for amount in amounts:
        interest_percentage = amount.interest_percentage
        borrowed_amount = amount.amount
        start_date = amount.start_date
        current_date = datetime.date.today()
        days = (current_date - start_date).days
        interest = borrowed_amount * days * (round(interest_percentage / 36500, 4))
        amount.interest_amount = interest
        amount.save()
