from rest_framework import routers

from api.views import FinanceUserViewSet, AmountDetailsViewSet, AgreementFileView
from django.urls import path

router = routers.SimpleRouter(trailing_slash=False)
router.register('users', FinanceUserViewSet, basename="users")
router.register('loans', AmountDetailsViewSet, basename="loans")


urlpatterns = [
    path('borrower/agreement_file', AgreementFileView.as_view(), )
]

urlpatterns += router.urls
