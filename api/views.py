import datetime
import pandas as pd

from rest_framework import viewsets, mixins, status

from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from django.http import HttpResponse

from django.views.generic import CreateView

from api.models import FinanceUser, AmountDetails
from api.serializers import FinanceUserSerializer, AmountDetailsSerializer, FileUploadSerializer
from api.forms import AgreementFileForm
from api.tasks import update_interest_task


class FinanceUserViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    queryset = FinanceUser.objects.all()
    serializer_class = FinanceUserSerializer

    @permission_classes([AllowAny])
    def create(self, request, *args, **kwargs):
        data = request.data
        data["username"] = data["email"]
        serializer = FinanceUserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AmountDetailsViewSet(viewsets.ModelViewSet):
    queryset = AmountDetails.objects.all()
    serializer_class = AmountDetailsSerializer

    def list(self, request, *args, **kwargs):
        user = request.user
        lent_amounts = AmountDetails.objects.filter(lender=user)
        update_interest_task(lent_amounts)
        lent_amounts = AmountDetails.objects.filter(lender=user)
        serialized_data = AmountDetailsSerializer(lent_amounts, many=True).data
        return Response(serialized_data, status=status.HTTP_200_OK)

    @action(methods=["PATCH"], detail=True)
    def close_loan(self, request, *args, **kwargs):
        loan = AmountDetails.objects.get(pk=kwargs.get("loan_id"))
        update_interest_task([loan])
        loan.is_closed = True
        loan.save()

        return Response("Successfully closed loan", status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        data = request.data
        interest_rate = data.get("interest_rate")
        interest_percentage = float(interest_rate) * 12
        data["interest_percentage"] = interest_percentage
        data["lender"] = request.user.id

        amount_serializer = AmountDetailsSerializer(data=data)
        if amount_serializer.is_valid():
            amount_serializer.save()
            return Response(amount_serializer.data, status=status.HTTP_201_CREATED)
        return Response(amount_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=["POST"], detail=False, permission_classes=[AllowAny, ])
    def calculate_interest(self, request, *args, **kwargs):
        data = request.data
        interest_amount = float(data.get("intrest")) * 12
        borrowed_amount = int(data.get("amount"))
        start_date = datetime.datetime.strptime(data.get("start_date"), '%Y-%m-%d')
        end_date = datetime.datetime.strptime(data.get("end_date"), '%Y-%m-%d')
        date_diff = end_date - start_date
        days = date_diff.days

        interest = borrowed_amount * days * (round(interest_amount / 36500, 4))
        return Response(
            {
                "interest": interest,
                "days": days,
                "amount": borrowed_amount,
                "intrest": data.get("intrest"),
                "start_date": data.get("start_date"),
                "end_date": data.get("end_date"),
            },
            status=status.HTTP_200_OK
        )

    @action(methods=["POST"], detail=False, url_path="csv_load")
    def load_loans(self, request, *args, **kwargs):
        file_serializer = FileUploadSerializer(data=request.data)
        if file_serializer.is_valid():
            file = file_serializer.validated_data["file"]
            model_obj = []
            data = pd.read_excel(file)
            for _, row in data.iterrows():
                interest_percentage = float(row["Interest Rate"]) * 12
                model_obj.append(AmountDetails(**{
                    "lender_mobile_number": row["Phone Number"],
                    "lended_to": row["Lended To"],
                    "amount": row["Amount"],
                    "start_date": str(row["Start Date"].date()),
                    "interest_rate": row["Interest Rate"],
                    "lender_id": request.user.id,
                    "interest_percentage": interest_percentage
                }))
            AmountDetails.objects.bulk_create(model_obj)
        return Response("uploaded")


class AgreementFileView(CreateView):
    def post(self, request, *args, **kwargs):
        borrower_id = int(request.POST.get('borrower_id'))
        borrower = AmountDetails.objects.get(id=borrower_id)
        
        agreement_form = AgreementFileForm(request.POST, request.FILES, instance=borrower)
        if agreement_form.is_valid():
            agreement_form.save()
            return HttpResponse("Success")
        return HttpResponse("Error", status=400)