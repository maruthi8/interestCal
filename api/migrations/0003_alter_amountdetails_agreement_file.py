# Generated by Django 3.2.7 on 2021-11-08 13:59

import api.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20211108_1140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='amountdetails',
            name='agreement_file',
            field=models.FileField(blank=True, null=True, upload_to=api.models.content_file_name),
        ),
    ]
