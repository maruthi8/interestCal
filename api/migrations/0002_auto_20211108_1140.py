# Generated by Django 3.2.7 on 2021-11-08 11:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='amountdetails',
            name='borrower_mobile_number',
            field=models.PositiveBigIntegerField(default=9999999999),
        ),
        migrations.AddField(
            model_name='amountdetails',
            name='closed_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
