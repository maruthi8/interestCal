from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from api.models import AmountDetails


# @receiver(post_save, sender=AmountDetails)
# def send_message(sender, instance, created, **kwargs):
#     if instance.is_closed:
#         message_to_broadcast = f"Thanks for paying loan amount, " \
#                                f"you have borrowed {instance.amount} on {str(instance.start_date)}, " \
#                                f"interest still date is {instance.interest_amount}"
#         client = messagebird.Client("323ir9BnDBtnpEm6uJzTfK8ao")
#         message = client.message_create(
#             'TestMessage',
#             '+918185819879',
#             message_to_broadcast,
#             {'reference': 'Foobar'}
#         )


# import urllib.request
# import urllib.parse


# @receiver(post_save, sender=AmountDetails)
# def send_sms(sender, instance, created, **kwargs):
#     if instance.is_closed:
#         number = instance.lender_mobile_number
#         message_to_broadcast = f"Thanks for paying loan amount, " \
#                                f"you have borrowed {instance.amount} on {str(instance.start_date)}, " \
#                                f"interest still date is {instance.interest_amount}"
#         data = urllib.parse.urlencode(
#             {
#                 'apikey': "Mzg1NDVhNmM3MDZlNjc0ZjZmNDY3MTU2Nzg1NzMwNGQ=",
#                 'numbers': '91' + str(number),
#                 'message': message_to_broadcast,
#                 'sender': "MARUTH"
#             }
#         )
#         data = data.encode('utf-8')
#         request = urllib.request.Request("https://api.textlocal.in/send/?")
#         f = urllib.request.urlopen(request, data)
#         fr = f.read()
#         print(fr)
