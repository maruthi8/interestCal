from django.forms import ModelForm

from api.models import AmountDetails


class AgreementFileForm(ModelForm):
    class Meta:
        fields = ("agreement_file", )
        model = AmountDetails
