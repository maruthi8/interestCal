import './App.css';
import { Container } from 'react-bootstrap';

import { HashRouter as Router, Route } from 'react-router-dom';

import HomeScreen from './redux/screens/HomeScreen';
import ResultScreen from './redux/screens/ResultScreen';
import LoginScreen from './redux/screens/LoginScreen';
import RegisterScreen from './redux/screens/RegisterScreen';
import Header from "./components/Header";
import MyBorrowersScreen from './redux/screens/MyBorrowersScreen';
import AddBorrowerScreen from './redux/screens/addBorrowerScreen';
import BorrowerEditScreen from './redux/screens/BorrowerEditScreen';

function App() {
  return (
    <Router>
      <Header />
      <main className="py-3">
          <Container>
            <Route path="/" component={HomeScreen} exact />
            <Route path="/success" component={ResultScreen} />
            <Route path="/login" component={LoginScreen} />
            <Route path="/register" component={RegisterScreen} />
            <Route path="/my_borrowers" component={MyBorrowersScreen} />
            <Route path="/borrowers/add" component={AddBorrowerScreen} />
            <Route path="/borrowers/edit/:id" component={BorrowerEditScreen} />


          </Container>
      </main>
    </Router>
  )
};

export default App;
