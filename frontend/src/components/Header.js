import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import { LinkContainer, Link } from 'react-router-bootstrap'
import { logout } from '../redux/actions/userActions'

function Header() {
    // const cart = useSelector(state => state.cart)
    // const { cartItems } = cart

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const dispatch = useDispatch()

    const logoutHandler = () => {
        dispatch(logout())
    }

    return (
            <Navbar bg="dark" variant="dark" sticky='top' collapseOnSelect>
                <Container>
                    <LinkContainer to='/'>
                        <Navbar.Brand>Loan Tracker</Navbar.Brand>
                    </LinkContainer>

                    <Navbar.Toggle aria-controls="basic-navbar-nav" />                    
                        <Nav className='mr-auto'>
                            <LinkContainer to='/my_borrowers'>
                                <Nav.Link >Money Lenders</Nav.Link>
                            </LinkContainer>

                        </Nav>
                    {userInfo ? (
                        <NavDropdown title={userInfo.username} id='basic-nav-dropdown'>
                            <LinkContainer to='/profile'>
                                <NavDropdown.Item>Profile</NavDropdown.Item>
                            </LinkContainer>

                            <NavDropdown.Item onClick={logoutHandler}>Logout</NavDropdown.Item>

                        </NavDropdown>
                    ) : (
                            <LinkContainer to='/login'>
                                <Nav.Link><i className="fas fa-user"></i>Login</Nav.Link>
                            </LinkContainer>
                        )}
                </Container>
                
            </Navbar>
    )
}

export default Header