import React, {useEffect, useState} from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Table, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Loader from '../../components/Loader'
import Message from '../../components/Message'
import { listBorrowers } from '../actions/borrowerActions'
import { Link } from 'react-router-dom'
import axios from 'axios'


function MyBorrowersScreen( { history }) {
    const dispatch = useDispatch()
    const [file, setFile] = useState({})
    const borrowerList = useSelector(state => state.borrowerList)
    const { loading, error, borrowers } = borrowerList

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    useEffect(() => {
        if (userInfo ) {
            dispatch(listBorrowers())
        } else {
            history.push('/login')
        }

    }, [dispatch, history, userInfo])

    const HandleSubmit = (e) => {
        history.push("/borrowers/add")
    }

    const onFileUpload = (e) => {
      // Create an object of formData
      const formData = new FormData();
      // Update the formData object
      formData.append("file", file);

      const config = {
            headers: {
                'Content-type': 'multipart/form-data',
                'Authorization': `Bearer ${userInfo.access}`
            }
        }
      axios.post(`/api/loans/csv_load`, formData, config);
      window.location.reload(true);
    };

    return (
        <div>
            <h1>Borrowers</h1>
            {loading
                ? (<Loader />)
                : error
                    ? (<Message variant='danger'>{error}</Message>)
                    : (
                        <Table striped bordered hover responsive className='table-sm'>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Borrower Name</th>
                                    <th>Phone Number</th>
                                    <th>Borrowed Date</th>
                                    <th>Actual Amount</th>
                                    <th>Interest Amount</th>
                                    <th>Total Amount</th>
                                    <th>Interest Rate</th>
                                    <th>Closed</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                {borrowers.map(borrower => (
                                    <tr key={borrower.id}>
                                        <td>{borrower.id}</td>
                                        <td>{borrower.lended_to}</td>
                                        <td>{borrower.borrower_mobile_number}</td>
                                        <td>{borrower.start_date.substring(0, 10)}</td>
                                        <td>{borrower.amount}</td>
                                        <td>{borrower.interest_amount}</td>
                                        <td>{borrower.interest_amount + borrower.amount}</td>
                                        <td>{borrower.interest_rate}</td>

                                        <td>{borrower.is_closed ? (
                                            <h5>InActive</h5>
                                        ) : (
                                            <h5>Active</h5>
                                            )}
                                        </td>

                                        <td>
                                            <LinkContainer to={`/borrowers/edit/${borrower.id}`}>
                                                <Button variant='primary' className='btn-sm'>
                                                    <i className='fas fa-edit'></i>
                                                </Button>
                                            </LinkContainer>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    )}
            <div>
                <Button type="click" variant="primary" onClick={HandleSubmit}>Add Borrower</Button>
                <div style={{"float": "right"}}>
                    <input type="file" onChange={(e) => setFile(e.target.files[0])}></input>
                    <Button type="click" onClick={onFileUpload}>Upload Excel</Button>
                </div>
            </div>
        </div>
    )
}

export default MyBorrowersScreen
