import React from 'react'
import { useSelector} from 'react-redux'
import { Form, Button } from 'react-bootstrap'

import FormContainer from '../../components/FormContainer'


function ResultScreen({ history }) {

    const interestResult = useSelector(state => state.interestResult)
    const { interest } = interestResult

    const submitHandler = (e) => {
        localStorage.removeItem('interestInfo')
        history.push("/")
    }

    return (
        <FormContainer>
            {interest ? (
                <div>
                    <h2>Check calculated interest</h2>
                <Form onSubmit={submitHandler}>
                    <Form.Group controlId='actual_amount'>
                        <Form.Label>Actual Amount</Form.Label>
                        <Form.Control
                            readOnly
                            type='actual_amount'
                            placeholder='Enter Actual Amount'
                            value={interest.amount}
                        >
                        </Form.Control>
                    </Form.Group>
    
                    <Form.Group controlId='interest'>
                        <Form.Label>Interest</Form.Label>
                        <Form.Control
                            readOnly
                            type='interest'
                            placeholder='Enter interest in rupees'
                            value={interest.intrest}
                        >
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId='start_date'>
                        <Form.Label>Start Date</Form.Label>
                        <Form.Control
                            readOnly
                            type='date'
                            placeholder='Enter Start Date in format dd/mm/yyyy'
                            value={interest.start_date}
                        >
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId='end_date'>
                        <Form.Label>End Date</Form.Label>
                        <Form.Control
                            readOnly
                            type='date'
                            placeholder='Enter End Date in format dd/mm/yyyy'
                            value={interest.end_date}
                        >
                        </Form.Control>
                    </Form.Group>
                    {interest ? (
                        <h4>Total interest for the period of {interest.days} days is {interest.interest} </h4>
                    ): null} 
    
                    <Button type='submit' variant='primary'>ReCalulate</Button>
                </Form>
                </div>
                
            ): (
                <Button type='submit' variant='primary'>Refresh</Button>
            )}
            
            
        </FormContainer>
    )
}


export default ResultScreen;