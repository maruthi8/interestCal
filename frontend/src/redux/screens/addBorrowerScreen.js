import React, { useState, useEffect } from 'react'
import FormContainer from '../../components/FormContainer'
import { useDispatch, useSelector} from 'react-redux'
import { Form, Button } from 'react-bootstrap'

import Loader from '../../components/Loader'
import Message from '../../components/Message'
import { createInterest } from '../actions/interestActions'


function AddBorrowerScreen ({ location, history }) {
    const [amount, setAmount] = useState(0)
    const [interest, setInterest] = useState(0.0)
    const [start_date, setStartDate] = useState('')
    const [lendedTo, setLendedTo] = useState('')
    const [b_mobile_number, setBMobileNumber] = useState(999999999)


    const dispatch = useDispatch()

    const redirect = location.search ? location.search.split('=')[1] : '/'

    const userLogin = useSelector(state => state.userLogin)
    const { error, loading, userInfo } = userLogin

    useEffect(() => {
        if (!userInfo) {
            history.push('/login')
        }
    }, [history, userInfo, redirect])

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(createInterest(
            {
                "amount": amount,
                "interest_rate": interest,
                "lended_to": lendedTo,
                "start_date": start_date,
                "borrower_mobile_number": b_mobile_number
            }
        ))
        history.push("/my_borrowers")
    }

    const CancelHandler = (e) => {
        e.preventDefault()
        history.push("/my_borrowers")
    }

    return (
        <FormContainer>
        <h1>Add Borrower</h1>
        {error && <Message variant='danger'>{error}</Message>}
        {loading && <Loader />}

        <Form onSubmit={submitHandler}>
            <Form.Group controlId='email'>
                <Form.Label>Amount</Form.Label>
                <Form.Control
                    type='integer'
                    placeholder='Enter Amount'
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>

            <Form.Group controlId='interest_rate'>
                <Form.Label>Interest</Form.Label>
                <Form.Control
                    type='float'
                    placeholder='Enter Interest in rupees'
                    value={interest}
                    onChange={(e) => setInterest(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>
            <Form.Group controlId='lended_to'>
                <Form.Label>Lended To</Form.Label>
                <Form.Control
                    type='text'
                    placeholder='Enter Borrower Name'
                    value={lendedTo}
                    onChange={(e) => setLendedTo(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>
            <Form.Group controlId='start_date'>
                <Form.Label>Start Date</Form.Label>
                <Form.Control
                    type='date'
                    placeholder='Enter Start Date in format dd/mm/yyyy'
                    value={start_date}
                    onChange={(e) => setStartDate(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>
            <Form.Group controlId='b_mobile_number'>
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type='integer'
                    placeholder='Enter Borrower Mobile Number'
                    value={b_mobile_number}
                    onChange={(e) => setBMobileNumber(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>

            <Button type='submit' variant='primary'>Add</Button>
            <Button style={{color: 'red', float: 'right'}} onClick={CancelHandler}>Cancel</Button>
        </Form>
    </FormContainer>
    )
}

export default AddBorrowerScreen;
