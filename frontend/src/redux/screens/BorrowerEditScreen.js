import React, {useState, useEffect} from 'react'
import { updateInterest } from '../actions/interestActions'
import { listBorrowerDetails } from '../actions/borrowerActions'
import FormContainer from '../../components/FormContainer'
import { useDispatch, useSelector} from 'react-redux'
import { Form, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import axios from 'axios'

import Loader from '../../components/Loader'
import Message from '../../components/Message'
import { BORROWER_DETAILS_RESET } from '../constants/borrowerConstants'

function BorrowerEditScreen({match, history}) {
    const [amount, setAmount] = useState(0)
    const [interestRate, setInterestRate] = useState(0.0)
    const [start_date, setStartDate] = useState('')
    const [lendedTo, setLendedTo] = useState('')
    const [b_mobile_number, setBMobileNumber] = useState(999999999)
    const [agreement_file, setAgreementFile] = useState('')
    const [uploading, setUploading] = useState(false)


    const borrowerId = match.params.id

    const dispatch = useDispatch()

    const borrowerDetails = useSelector(state => state.borrowerDetails)
    const { borrower } = borrowerDetails
    console.log(Number(borrower.id) !== Number(borrowerId))

    const interestUpdate = useSelector(state => state.interestUpdate)
    const { error, loading, success: successUpdate } = interestUpdate

    useEffect(() => {

        if (successUpdate) {
            dispatch({ type: BORROWER_DETAILS_RESET })
            history.push('/my_borrowers')
        } else {
            if ( Number(borrower.id) !== Number(borrowerId)) {
                dispatch(listBorrowerDetails(borrowerId))
            } else {
                setAmount(borrower.amount)
                setInterestRate(borrower.interest_rate)
                setStartDate(borrower.start_date)
                setLendedTo(borrower.lended_to)
                setBMobileNumber(borrower.borrower_mobile_number)
            }
        }

    }, [dispatch, borrower, borrowerId, history, successUpdate])
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    const csrftoken = getCookie('csrftoken');
    const uploadFileHandler = async (e) => {
        const file = e.target.files[0]
        const formData = new FormData()

        formData.append('agreement_file', file)
        formData.append('borrower_id', borrowerId)

        setUploading(true)

        try {
            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'X-CSRFToken': csrftoken
                }
            }

            const { data } = await axios.post('/api/borrower/agreement_file', formData, config)


            setAgreementFile(data)
            setUploading(false)
        } catch (error) {
            setUploading(false)
        }
    }

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(updateInterest(
            {
                id: borrowerId,
                amount: amount,
                interest_rate: interestRate,
                lended_to: lendedTo,
                start_date: start_date,
                borrower_mobile_number: b_mobile_number
            }
        ))
    }

    return (
        <div>
            <Link to='/my_borrowers'>
                Go Back
            </Link>
        <FormContainer>
        <h1>Edit Borrower</h1>
        {error && <Message variant='danger'>{error}</Message>}
        {loading && <Loader />}

        <Form onSubmit={submitHandler}>
            <Form.Group controlId='email'>
                <Form.Label>Amount</Form.Label>
                <Form.Control
                    type='integer'
                    placeholder='Enter Amount'
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>

            <Form.Group controlId='interest_rate'>
                <Form.Label>Interest</Form.Label>
                <Form.Control
                    type='float'
                    placeholder='Enter Interest in rupees'
                    value={interestRate}
                    onChange={(e) => setInterestRate(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>
            <Form.Group controlId='lended_to'>
                <Form.Label>Lended To</Form.Label>
                <Form.Control
                    type='text'
                    placeholder='Enter Borrower Name'
                    value={lendedTo}
                    onChange={(e) => setLendedTo(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>
            <Form.Group controlId='start_date'>
                <Form.Label>Start Date</Form.Label>
                <Form.Control
                    type='date'
                    placeholder='Enter Start Date in format dd/mm/yyyy'
                    value={start_date}
                    onChange={(e) => setStartDate(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>
            <Form.Group controlId='b_mobile_number'>
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type='integer'
                    placeholder='Enter Borrower Mobile Number'
                    value={b_mobile_number}
                    onChange={(e) => setBMobileNumber(e.target.value)}
                    required={true}
                >
                </Form.Control>
            </Form.Group>
            <br></br>
            <Form.Group controlId='agreement_file'>
                <Form.Label>Agreement File</Form.Label>
                {/* <Form.Control
                    type='text'
                    placeholder='Upload File'
                    value={agreement_file}
                    onChange={(e) => setAgreementFile(e.target.value)}
                >
                </Form.Control> */}

                <Form.File
                    id='agreement-file'
                    custom
                    onChange={uploadFileHandler}
                >

                </Form.File>
                {uploading && <Loader />}

            </Form.Group>

            <Button type='submit' variant='primary'>Update</Button>
        </Form>
    </FormContainer>

    </div>
    )
}

export default BorrowerEditScreen
