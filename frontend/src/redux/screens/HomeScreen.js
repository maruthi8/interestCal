import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector} from 'react-redux'
import { Row, Col, Form, Button } from 'react-bootstrap'

import { Link } from 'react-router-dom'
import Loader from '../../components/Loader'
import Message from '../../components/Message'
import FormContainer from '../../components/FormContainer'

import { getInterest } from '../actions/interestActions'

function HomeScreen({ location, history }) {

    const [amount, setAmount] = useState('')
    const [interest, setInterest] = useState('')
    const [start_date, setStartDate] = useState('')
    const [end_date, setEndDate] = useState('')


    const dispatch = useDispatch()

    const redirect = location.search ? location.search.split('=')[1] : '/'

    const userLogin = useSelector(state => state.userLogin)
    const { error, loading, userInfo } = userLogin
    
    window.onload = function () {
        console.log("OnLoad")
        localStorage.removeItem('interestInfo');
    };

    useEffect(() => {
        if (userInfo) {
            history.push(redirect)
        }
    }, [history, userInfo, redirect])

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(getInterest(amount, interest, start_date, end_date))
        history.push("/success")
    }

    return (
        <FormContainer>
            <h1>Calculate Interest</h1>
            {error && <Message variant='danger'>{error}</Message>}
            {loading && <Loader />}

            <Form onSubmit={submitHandler}>
                <Form.Group controlId='actual_amount'>
                    <Form.Label>Actual Amount</Form.Label>
                    <Form.Control
                        type='int32'
                        placeholder='Enter Actual Amount'
                        value={amount}
                        onChange={(e) => setAmount(e.target.value)}
                        required={true}
                    >
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='interest'>
                    <Form.Label>Interest</Form.Label>
                    <Form.Control
                        type='float'
                        placeholder='Enter interest in rupees'
                        value={interest}
                        onChange={(e) => setInterest(e.target.value)}
                        required={true}
                    >
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId='start_date'>
                    <Form.Label>Start Date</Form.Label>
                    <Form.Control
                        type='date'
                        placeholder='Enter Start Date in format dd/mm/yyyy'
                        value={start_date}
                        onChange={(e) => setStartDate(e.target.value)}
                        required={true}
                    >
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId='end_date'>
                    <Form.Label>End Date</Form.Label>
                    <Form.Control
                        type='date'
                        placeholder='Enter End Date in format dd/mm/yyyy'
                        value={end_date}
                        onChange={(e) => setEndDate(e.target.value)}
                        required={true}
                    >
                    </Form.Control>
                </Form.Group>
                <br></br>
                <Button type='submit' variant='primary'>Calculate</Button>
            </Form>
            <br></br>
            {
                !userInfo ?(
                    <Row className="py-3">
                        <Col>
                            If you like to store you lenders data and get notfied about dates please click here to login? <Link
                                to={'/login'}>
                                    Login
                                </Link>
                        </Col>
                    </Row>
                ): null}
            
        </FormContainer>
    )
}


export default HomeScreen;