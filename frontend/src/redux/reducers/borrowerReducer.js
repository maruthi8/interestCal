import {
    BORROWER_LIST_REQUEST,
    BORROWER_LIST_SUCCESS,
    BORROWER_LIST_FAIL,

    BORROWER_FILTER_LIST_REQUEST,
    BORROWER_FILTER_LIST_SUCCESS,
    BORROWER_FILTER_LIST_FAIL,

    BORROWER_DETAILS_REQUEST,
    BORROWER_DETAILS_SUCCESS,
    BORROWER_DETAILS_FAIL
} from '../constants/borrowerConstants'


export const borrowerListReducer = (state = { borrowers:[] }, action) => {
    switch(action.type){
        case BORROWER_LIST_REQUEST:
            return { loading:true, borrowers: [] }

        case BORROWER_LIST_SUCCESS:
            return {
                loading:false,
                borrowers: action.payload,
            }

        case BORROWER_LIST_FAIL:
            return { loading:false, error: action.payload }

        default:
            return state
    }
}

export const borrowerFilterListReducer = (state = { borrowers:[] }, action) => {
    switch(action.type){
        case BORROWER_FILTER_LIST_REQUEST:
            return { loading:true, borrowers: [] }

        case BORROWER_FILTER_LIST_SUCCESS:
            return {
                loading:false,
                borrowers: action.payload,
            }

        case BORROWER_FILTER_LIST_FAIL:
            return { loading:false, error: action.payload }

        default:
            return state
    }
}

export const borrowerDetailsReducer = (state = { borrower:{} }, action) => {
    switch(action.type){
        case BORROWER_DETAILS_REQUEST:
            return { loading:true, ...state }

        case BORROWER_DETAILS_SUCCESS:
            return { loading:false, borrower: action.payload }

        case BORROWER_DETAILS_FAIL:
            return { loading:false, error: action.payload }

        default:
            return state
    }
}
