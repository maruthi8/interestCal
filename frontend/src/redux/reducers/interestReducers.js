import {
    INTEREST_LIST_REQUEST,
    INTEREST_LIST_SUCCESS,
    INTEREST_LIST_FAIL,

    INTEREST_FILTER_LIST_REQUEST,
    INTEREST_FILTER_LIST_SUCCESS,
    INTEREST_FILTER_LIST_FAIL,

    INTEREST_DETAILS_REQUEST,
    INTEREST_DETAILS_SUCCESS,
    INTEREST_DETAILS_FAIL,

    INTEREST_DELETE_REQUEST,
    INTEREST_DELETE_SUCCESS,
    INTEREST_DELETE_FAIL,

    INTEREST_CREATE_REQUEST,
    INTEREST_CREATE_SUCCESS,
    INTEREST_CREATE_FAIL,
    INTEREST_CREATE_RESET,

    INTEREST_UPDATE_REQUEST,
    INTEREST_UPDATE_SUCCESS,
    INTEREST_UPDATE_FAIL,
    INTEREST_UPDATE_RESET,

    INTEREST_CALCULATE_REQUEST,
    INTEREST_CALCULATE_SUCCESS,
    INTEREST_CALCULATE_FAIL,
    INTEREST_CALCULATE_RESET,
 } from '../constants/InterestConstants'


export const interestListReducer = (state = { interests:[] }, action) => {
    switch(action.type){
        case INTEREST_LIST_REQUEST:
            return { loading:true, interests: [] }

        case INTEREST_LIST_SUCCESS:
            return {
                loading:false,
                interests: action.payload,
            }

        case INTEREST_LIST_FAIL:
            return { loading:false, error: action.payload }

        default:
            return state
    }
}

export const interestFilterListReducer = (state = { interests:[] }, action) => {
    switch(action.type){
        case INTEREST_FILTER_LIST_REQUEST:
            return { loading:true, interests: [] }

        case INTEREST_FILTER_LIST_SUCCESS:
            return {
                loading:false,
                interests: action.payload,
            }

        case INTEREST_FILTER_LIST_FAIL:
            return { loading:false, error: action.payload }

        default:
            return state
    }
}

export const interestDetailsReducer = (state = { interest_details:{} }, action) => {
    switch(action.type){
        case INTEREST_DETAILS_REQUEST:
            return { loading:true, ...state }

        case INTEREST_DETAILS_SUCCESS:
            return { loading:false, interest_details: action.payload }

        case INTEREST_DETAILS_FAIL:
            return { loading:false, error: action.payload }

        default:
            return state
    }
}

export const interestUpdateReducer = (state = { interest: {} }, action) => {
    switch (action.type) {
        case INTEREST_UPDATE_REQUEST:
            return { loading: true }

        case INTEREST_UPDATE_SUCCESS:
            return { loading: false, success: true, interest: action.payload }

        case INTEREST_UPDATE_FAIL:
            return { loading: false, error: action.payload }

        case INTEREST_UPDATE_RESET:
            return { interest: {} }

        default:
            return state
    }
}

export const interestDeleteReducer = (state = { }, action) => {
    switch(action.type){
        case INTEREST_DELETE_REQUEST:
            return { loading:true }

        case INTEREST_DELETE_SUCCESS:
            return { loading:false, success: true }

        case INTEREST_DELETE_FAIL:
            return { loading:false, error: action.payload }

        default:
            return state
    }
}

export const interestCreateReducer = (state = { }, action) => {
    switch(action.type){
        case INTEREST_CREATE_REQUEST:
            return { loading:true }

        case INTEREST_CREATE_SUCCESS:
            return { loading:false, success: true, interest:action.payload }

        case INTEREST_CREATE_FAIL:
            return { loading:false, error: action.payload }
        
        case INTEREST_CREATE_RESET:
            return {}

        default:
            return state
    }
}

export const interestCalReducer = (state = { }, action) => {
    switch(action.type){
        case INTEREST_CALCULATE_REQUEST:
            return { loading:true }

        case INTEREST_CALCULATE_SUCCESS:
            return { loading:false, success: true, interest:action.payload }

        case INTEREST_CALCULATE_FAIL:
            return { loading:false, error: action.payload }
        
        case INTEREST_CALCULATE_RESET:
            return {}

        default:
            return state
    }
}
