import axios from 'axios'

import {
    BORROWER_LIST_REQUEST,
    BORROWER_LIST_SUCCESS,
    BORROWER_LIST_FAIL,

    BORROWER_DETAILS_REQUEST,
    BORROWER_DETAILS_SUCCESS,
    BORROWER_DETAILS_FAIL,

 } from '../constants/borrowerConstants'

 export const listBorrowers = (keyword) => async (dispatch, getState) => {

    try{
        dispatch({ type: BORROWER_LIST_REQUEST })
        const {
            userLogin: { userInfo },
        } = getState()

        const config = {
            headers: {
                'Content-type': 'Application/json',
                'Authorization': `Bearer ${userInfo.access}`
            }
        }

        const { data } = await axios.get(`api/loans`, config)

        dispatch({
            type: BORROWER_LIST_SUCCESS,
            payload: data
        })

    }catch(error){
        dispatch({
            type: BORROWER_LIST_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
 }


 export const listBorrowerDetails = (id) => async (dispatch, getState) => {
    try{
        dispatch({ type: BORROWER_DETAILS_REQUEST })
        const {
            userLogin: { userInfo },
        } = getState()

        const config = {
            headers: {
                'Content-type': 'Application/json',
                'Authorization': `Bearer ${userInfo.access}`
            }
        }
        const { data } = await axios.get(`/api/loans/${id}`, config)

        dispatch({
            type: BORROWER_DETAILS_SUCCESS,
            payload: data
        })

    }catch(error){
        dispatch({
            type: BORROWER_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
 }