import axios from 'axios'

import {
    INTEREST_LIST_REQUEST,
    INTEREST_LIST_SUCCESS,
    INTEREST_LIST_FAIL,

    INTEREST_FILTER_LIST_REQUEST,
    INTEREST_FILTER_LIST_SUCCESS,
    INTEREST_FILTER_LIST_FAIL,

    INTEREST_DETAILS_REQUEST,
    INTEREST_DETAILS_SUCCESS,
    INTEREST_DETAILS_FAIL,

    INTEREST_DELETE_REQUEST,
    INTEREST_DELETE_SUCCESS,
    INTEREST_DELETE_FAIL,

    INTEREST_CREATE_REQUEST,
    INTEREST_CREATE_SUCCESS,
    INTEREST_CREATE_FAIL,

    INTEREST_UPDATE_REQUEST,
    INTEREST_UPDATE_SUCCESS,
    INTEREST_UPDATE_FAIL,

    INTEREST_CALCULATE_REQUEST,
    INTEREST_CALCULATE_SUCCESS,
    INTEREST_CALCULATE_FAIL,
 } from '../constants/InterestConstants'

 export const listInterests = (keyword) => async (dispatch) => {

    try{
        dispatch({ type: INTEREST_LIST_REQUEST })

        const config = {
            headers: {
                'Content-type': 'Application/json',
            }
        }

        const { data } = await axios.get(`/api/interests${keyword}`, config)

        dispatch({
            type: INTEREST_LIST_SUCCESS,
            payload: data
        })

    }catch(error){
        dispatch({
            type: INTEREST_LIST_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
 }

 export const listFilterInterests = (keyword) => async (dispatch) => {

    try{
        dispatch({ type: INTEREST_FILTER_LIST_REQUEST })

        const config = {
            headers: {
                'Content-type': 'Application/json',
            }
        }
        const { data } = await axios.get(`/api/interests${keyword}`, config)

        dispatch({
            type: INTEREST_FILTER_LIST_SUCCESS,
            payload: data
        })

    }catch(error){
        dispatch({
            type: INTEREST_FILTER_LIST_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
 }

 export const createInterest = (interest) => async (dispatch, getState) => {
    try {
        dispatch({
            type: INTEREST_CREATE_REQUEST

        })

        const {
            userLogin: { userInfo },
        } = getState()

        const config = {
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${userInfo.access}`
            }
        }

        const { data } = await axios.post(
            'api/loans',
            interest,
            config
        )

        dispatch({
            type: INTEREST_CREATE_SUCCESS,
            payload: data
        })

    }catch(error){
        dispatch({
            type: INTEREST_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

 export const listInterestDetails = (id) => async (dispatch, getState) => {
    try{
        dispatch({ type: INTEREST_DETAILS_REQUEST })
        const {
            userLogin: { userInfo },
        } = getState()

        const config = {
            headers: {
                'Content-type': 'Application/json',
                'Authorization': `Bearer ${userInfo.access}`
            }
        }
        const { data } = await axios.get(`/api/loans/${id}`, config)

        dispatch({
            type: INTEREST_DETAILS_SUCCESS,
            payload: data
        })

    }catch(error){
        dispatch({
            type: INTEREST_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
 }

 export const deleteInterest = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: INTEREST_DELETE_REQUEST
        })

        const {
            userLogin: { userInfo },
        } = getState()

        const config = {
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.delete(
            `/api/admin/interests/${id}`,
            config
        )

        dispatch({
            type: INTEREST_DELETE_SUCCESS,
            payload: data
        })


    } catch (error) {
        dispatch({
            type: INTEREST_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}


export const getInterest = (amount, interest, start_date, end_date) => async (dispatch, getState) => {
    try {
        dispatch({
            type: INTEREST_CALCULATE_REQUEST
        })

        const {
            userLogin: { userInfo },
        } = getState()
        const req_data = {
            "amount": amount,
            "intrest": interest,
            "start_date": start_date,
            "end_date": end_date,
        }
        console.log(req_data)

        const config = {
            headers: {
                'Content-type': 'application/json',
            }
        }
        console.log(config)

        const { data } = await axios.post(
            `/api/loans/calculate_interest`,
            req_data,
            config,
        )

        dispatch({
            type: INTEREST_CALCULATE_SUCCESS,
            payload: data
        })
        localStorage.setItem('interestInfo', JSON.stringify(data))


    } catch (error) {
        dispatch({
            type: INTEREST_CALCULATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}


export const updateInterest = (interest) => async (dispatch, getState) => {
    try {
        dispatch({
            type: INTEREST_UPDATE_REQUEST
        })

        const {
            userLogin: { userInfo },
        } = getState()

        const config = {
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${userInfo.access}`
            }
        }

        const { data } = await axios.patch(
            `/api/loans/${interest.id}`,
            interest,
            config
        )
        dispatch({
            type: INTEREST_UPDATE_SUCCESS,
            payload: data,
        })


        dispatch({
            type: INTEREST_DETAILS_SUCCESS,
            payload: data
        })


    } catch (error) {
        dispatch({
            type: INTEREST_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}
