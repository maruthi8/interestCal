import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import { composeWithDevTools } from 'redux-devtools-extension'
import {
    interestListReducer,
    interestDetailsReducer,
    interestDeleteReducer,
    interestCreateReducer,
    interestUpdateReducer,
    interestCalReducer
} from './reducers/interestReducers'
import {
    userLoginReducer,
    userRegisterReducer,
    // userDetialsReducer,
    // userUpdateProfileReducer,
    // userListReducer,
    // userDeleteReducer,
    // userUpdateReducer,
    // myProfileDetialsReducer,
    // userEmailVerifyReducer,
} from './reducers/userReducers'
import {
    borrowerListReducer,
    borrowerDetailsReducer
} from './reducers/borrowerReducer'

const reducer = combineReducers({
    interestList: interestListReducer,
    // interestsFilter: interestFilterListReducer,
    interestDetails: interestDetailsReducer,
    interestDelete: interestDeleteReducer,
    interestCreate: interestCreateReducer,
    interestUpdate: interestUpdateReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    interestResult: interestCalReducer,
    borrowerList: borrowerListReducer,
    borrowerDetails: borrowerDetailsReducer,
})

const userInfoFromStorage = localStorage.getItem('userInfo') ?
    JSON.parse(localStorage.getItem('userInfo')) : null

const interestInfoFromStorage = localStorage.getItem('interestInfo') ?
    JSON.parse(localStorage.getItem('interestInfo')) : null

const initialState = {
    userLogin: { userInfo: userInfoFromStorage },
    interestResult: {interest: interestInfoFromStorage}
}
const middleware = [thunk]

const store = createStore(reducer, initialState,
    composeWithDevTools(applyMiddleware(...middleware)))

export default store